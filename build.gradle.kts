plugins {
    java
    kotlin("jvm") version "1.8.10"
    id("artfable.artifact") version "0.0.4"
    `maven-publish`
}

group = "com.artfable.telegram"
version = "1.1.0"

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

val kotlin_version = "1.8.10"
val jackson_version = "2.15.0"
val slf4j_version = "2.0.7"
val javax_annotation_version = "1.3.2"

val junit_version = "5.9.3"
val mockito_version = "5.3.1"

dependencies {
    api("javax.annotation:javax.annotation-api:$javax_annotation_version")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    api("com.fasterxml.jackson.module:jackson-module-kotlin:$jackson_version")
    api("org.slf4j:slf4j-api:$slf4j_version")

    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
    testImplementation("org.mockito:mockito-junit-jupiter:$mockito_version")
}

repositories {
    mavenLocal()
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "17"
    }
    test {
        useJUnitPlatform()
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            artifact(tasks["sourceJar"])
            artifact(tasks["javadocJar"])
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()

            pom {
                description.set("API for Telegram bots")
                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://raw.githubusercontent.com/artfable/telegram-api-java/master/LICENSE")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("artfable")
                        name.set("Artem Veselov")
                        email.set("art-fable@mail.ru")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/46918453/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
