rootProject.name = "telegram-api"

pluginManagement {
    pluginManagement {
        repositories {
            gradlePluginPortal()
            mavenLocal()
            mavenCentral()
            maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
        }
    }
}

